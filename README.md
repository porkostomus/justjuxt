# Cryogen Now!

Because friends don't let friends use Jekyll. Or Github...

## To set up your Cryogen site:

1. Fork this project, then go to your settings and `Remove fork relationship`.
2. Edit `resources/templates/config.edn` to configure site with your name, etc.
3. `resources/templates/themes/blue/html/page.html` is your main template where you can change your links and stuff.
4. Edit the files in `resources/templates/md/posts` to make your posts!

Your site is rebuilt every time you edit a file and it's really fun to watch. View the console activity by clicking on the progress icon.